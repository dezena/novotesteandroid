package com.example.gabrielpdezena.appsimples;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.appsee.Appsee;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        Appsee.start(getString(R.string.com_appsee_apikey));
        setContentView(R.layout.activity_main);
    }
}
